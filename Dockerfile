FROM wavefier/python:3.6.8-jessie

ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8
WORKDIR /app

RUN apt-get --force-yes update
#RUN apt-get --assume-yes install librdkafka1
RUN apt-get --assume-yes install librdkafka-dev

COPY .  /app/.
RUN python setup.py develop

# CMD ["python", "-u", "wavefier_trigger_handlers/cli/RunTriggerListener.py"]
# CMD ["/bin/bash"] 