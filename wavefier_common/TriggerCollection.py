import logging

from wavefier_common.param.WdfParam import WdfParam
from wavefier_common.param.WitheningParam import WitheningParam
from wavefier_common.Trigger import Trigger


class TriggerCollection(object):
    """This class contain a Collection of trigger"""

    def __init__(self, channel: str = "unknown", wthParam: WitheningParam = None, wdfParam: WdfParam  = None, logger=None):
        """TriggerCollection
        
        Keyword Arguments:
            channel {str} -- [The channel] (default: {"unknown"})
            startGPSSegment {float} -- [GPS Start Time of current segment] (default: {0.0})
            wthParam {WitheningParam} -- [The used WitheningParam] (default: {WitheningParam()})
            wdfParam {WdfParam} -- [The used WdfParam ] (default: {WdfParam()})
        """
        self.logger = logger or logging.getLogger(__name__)

        if wthParam is None:
            raise ValueError("Inappropriate argument WitheningParam id object (of correct type)")

        if wthParam is None:
            raise ValueError("Inappropriate argument wdfParam id object (of correct type)")


        self.__channel = channel
        self.__GPSstart = 0
        self.__wthParam = wthParam
        self.__wdfParam = wdfParam
        self.__triggers = list()
        self.__triggers_count = 0

    def __repr__(self):
        return str(self.__dict__)

    def getChannel(self) -> str:
        """The Channel Name
        
        Returns:
            str -- The Channel
        """

        return self.__channel

    def getGPSSegment(self) -> float:
        """The Start GPS time of segment
        
        Returns:
            float -- GPS time
        """

        return self.__GPSstart

    def getWTHObject(self):
        return self.__wthParam


    def getWDFObject(self):
        return self.__wdfParam

    def getWTHParam(self, param):
        """Return the specific Withening paramenter
        
        Arguments:
            param {str} -- The parameter 
        
        Raises:
            ValueError -- If the paramente is not preset
        
        Returns:
            [type] -- the result
        """

        dicParam = self.__wthParam.__dict__
        if param in dicParam:
            return dicParam[param]
        else:
            self.logger.warning('Withening Paramenter missing: ', param)
            raise ValueError('Withening Paramenter ' + param + ' missing')

    def getWDFParam(self, param):
        """Return the specific WDF paramenter
        
        Arguments:
            param {str} -- The parameter 
        
        Raises:
            ValueError -- If the paramente is not preset
        
        Returns:
            [type] -- the result
        """

        dicParam = self.__wdfParam.__dict__
        if param in dicParam:
            return dicParam[param]
        else:
            self.logger.warning('WDF Paramenter missing: ', param)
            raise ValueError('WDF Paramenter missing')

    def count(self) -> int:
        """REturn the count of trigger
        
        Returns:
            int -- [description]
        """

        return self.__triggers_count

    def add(self, trigger: Trigger):
        """Add a single Trigger
        
        Arguments:
            trigger {Trigger} -- [description]
        """

        self.__triggers.append(trigger)
        self.__triggers_count += 1
        timeg=float(trigger.getGPS())
        if timeg < self.__GPSstart:
            self.__GPSstart=timeg

    def adds(self, triggers: list):
        """Add list of trigger

        Arguments:
            triggers {list} -- [description]
        """
        for trigger in triggers:
            self.add(trigger)

    def iterator(self) -> iter:
        """return a iterator of trigger
        
        Returns:
            iter -- [description]
        """

        return iter(self.__triggers)


    def __eq__(self, other):
        """Overrides the default implementation"""
        if isinstance(other, TriggerCollection):
            checkA = self.getChannel() == other.getChannel() \
                   and self.getGPSSegment() == other.getGPSSegment() \
                   and self.getWTHObject() == other.getWTHObject() \
                   and self.getWDFObject() == other.getWDFObject() \
                   and self.count() == other.count()
            if checkA:
                oIt=other.iterator()
                for trigger in self.iterator():
                    if not trigger.equal(oIt):
                        return False
                return True

        return False
