from wavefier_common.util.Time import Time
import uuid

class Trigger(object):
    """This class contains the Trigger information

    """

    def __init__(self,WDFParam_id : str = None, WTHParam_id : str = None,  gps: float = 0.0, snr: float = 0.0, snrMax: float = 0.0, freq: float = 0.0, freqMax: float = 0.0,
                 duration: float = 0.0, wave="-", nCoeff: int = 512, partition: int = 1,
                 wt: map = {}, rw: dict = {}, channel: str = 'Unamed Channel'):
        """This class contains the Trigger information

        Keyword Arguments:
            gps {float} -- [GPS Time of trigger] (default: {0.0})
            snr {float} -- [SNR value] (default: {0.0})
            snrMax {float} -- [SNR Max value] (default: {0.0})
            freq {float} -- [Freq value] (default: {0.0})
            freqMax {float} -- [FreqMax value] (default: {0.0})
            duration {float} -- [Duration value] (default: {0.0})
            wave {str} -- [Wave name] (default: {"-"})
            nCoeff {int} -- [Window Size] (default: {512})
            wt {dict} -- [Wavelet Coefficients] (default: {None})
            rw {dict} -- [Raw Wavelets] (default: {None})
        """
        if WDFParam_id is None or not  isinstance(WDFParam_id, str) :
            raise ValueError("Inappropriate argument WDFParam_id  (of correct type)")

        if WTHParam_id is None or not isinstance(WTHParam_id, str) :
            raise ValueError("Inappropriate argument WTHParam_id  (of correct type)")

        self.uuid = str(uuid.uuid4())
        self.WDFParam_id = WDFParam_id
        self.WTHParam_id = WTHParam_id
        self.channel = channel
        self.partition = int(partition)
        self.gps = float(gps)
        self.snr = float(snr)
        self.snrMax = float(snrMax)
        self.freq = float(freq)
        self.freqMax = float(freqMax)
        self.duration = float(duration)
        self.wave = wave
        self.wt = wt
        self.rw = rw
        self.nCoef = nCoeff


    def make_from_dic(self,dictionary):
        object=Trigger('dummy','dummy')
        for key in dictionary:
            setattr(object, key, dictionary[key])
        return object

    def __repr__(self):
        return str(Time.gps2UTCstr(self.gps)) + " (GPS Time: " + str(self.gps) + ") SNR:" + str(
            self.snr) + " SNRMax:" + str(self.snrMax) + " Freq:" + str(self.freq) + " MAXFreq:" + str(
            self.freqMax) + " Duration:" + str(self.duration) + " Wave:" + str(self.wave) + " NCoef:" + str(
            self.nCoef)

    def getGPS(self) -> float:
        """Get gps time

        Returns:
            float -- [the gps time]
        """

        return self.gps

    def getSNR(self) -> float:
        """
        Get SNR

        Returns:
            float -- [the SNR]
        """

        return self.snr

    def getMaxSNR(self) -> float:
        """
        The Max SNR

        Returns:
            float -- Max SNR
        """

        return self.snrMax

    def getFreq(self) -> float:
        """

        Returns:
            float --
        """

        return self.freq

    def getMaxFreq(self) -> float:
        """

        Returns:
            float --
        """
        return self.freqMax

    def getDuration(self) -> float:
        """
        
        Returns:
            float -- 
        """
        return self.duration

    def getWave(self) -> str:
        """
        
        Returns:
            str -- 
        """
        return self.wave

    def getWT(self, i: int) -> float:
        """
        Get the i-esimo WT value 
        
        Arguments:
            i {int} -- i-esimo
        
        Returns:
            float -- WT value
        """
        if self.wt == None:
            return 0.0
        else:
            return self.wt.get(str(i), 0.0)

    def getWT_all(self) -> dict:
        """
        Get the WT dictionary

        Returns:
            dict -- WT dict
        """

        return self.wt

    def getRW(self, i: int) -> float:
        """
            Get the i-esimo RW value 

        Arguments:
            i {int} -- i-esimo
        
        Returns:
            float -- value
        """
        if self.rw == None:
            return 0.0
        else:
            return self.rw.get(str(i), 0.0)

    def getRW_all(self) -> dict:
        """
        Get the RW dict

        Returns:
            dict -- RW dict
        """

        return self.rw

    def getNcoef(self) -> int:
        """The dimension of window
        
        Returns:
            int -- the value
        """
        return self.nCoef

    def getUUID(self) -> str:
        """The dimension of window

        Returns:
            int -- the value
        """
        return self.uuid

    def setWT(self, i: int, val: float):
        if val != 0.0:
            self.wt[str(i)] = val

    def setRW(self, i: int, val: float):
        if val != 0.0:
            self.rw[str(i)] = val

    def getPartition(self):
        """
        Get the current Trigger ID
        :return: integer
        """
        return self.partition

    def rewritePartitio(self, partition: int):
        """
        This function rewrite the ID of trigger
        :param id: integer
        """
        self.partition = partition

    def setChannel(self,channel):
        self.channel = channel

    def getChannel(self):
        return self.channel

    def __eq__(self, other):
        """Overrides the default implementation"""
        if isinstance(other, Trigger):
            checkA = self.partition == other.getPartition() \
                     and self.channel == other.getChannel() \
                     and self.gps == other.getGPS() \
                     and self.snr == other.getSNR() \
                     and self.snrMax == other.getMaxSNR() \
                     and self.freq == other.getFreq() \
                     and self.freqMax == other.getMaxFreq() \
                     and self.duration == other.getDuration() \
                     and self.wave == other.getWave() \
                     and self.nCoef == other.getNcoef() \
                     and self.wt == other.getWT_all() \
                     and self.rw == other.getRW_all() \
                     and self.uuid == other.getUUID() \
                     and self.WTHParam_id == other.WTHParam_id \
                     and self.WDFParam_id == other.WDFParam_id
            if checkA:
                if self.rw == None and self.wt == None :
                    return True
                else:
                    for count in range(0, self.getNcoef()):
                        if self.getWT(count) != other.getWT(count) or self.getRW(count) != other.getRW(count):
                            return False
                    return True
        return False
