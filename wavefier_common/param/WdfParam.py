from wavefier_common.kafka.Message import KafkaMessage

import logging
import uuid


class WdfParam(object):
    """This class contains the basic WDF configuration that can be used to characterize the trigger

    """

    def __init__(self, window: int = 1024, overlap: int = 16, threshold: float = 2.0,
                 BLfilterOrder: int = 9, ResamplingFactor: int = 4, learn: int = 300):
        """This class contains the basic WDF configuration that can be used to characterize the trigger

        Keyword Arguments:
            window {int} -- [The window size] (default: {1024})
            overla[ {int} -- [The overlap Value] (default: {16})
            threshold {float} -- [The threshold value] (default: {2.0})
            BLfilterOrder {int} -- [The BL Interpolation parameter] (default: {9})
            ResamplingFactor {int} -- [The resampling factor] (default: {4})
            learn {int} -- [The learning parameter] (default: {300})
        """

        self.uuid = str(uuid.uuid4())
        self.window = window
        self.overlap = overlap
        self.Ncoeff = window
        self.threshold = threshold
        self.BLfilterOrder = BLfilterOrder
        self.ResamplingFactor = ResamplingFactor
        self.learn = learn

    def __repr__(self):
        return str(self.__dict__)

    def __eq__(self, other):
        """Overrides the default implementation"""
        if isinstance(other, WdfParam):
            return self.window == other.window \
                   and self.overlap == other.overlap \
                   and self.Ncoeff == other.Ncoeff \
                   and self.threshold == other.threshold \
                   and self.BLfilterOrder == other.BLfilterOrder \
                   and self.ResamplingFactor == other.ResamplingFactor \
                   and self.learn == other.learn
        return False

    def make_from_dic(self, dictionary):
        object = WdfParam()
        for key in dictionary:
            setattr(object, key, dictionary[key])

        object.Ncoeff = object.window
        return object
