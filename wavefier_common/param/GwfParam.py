from wavefier_common.kafka.Message import KafkaMessage


class GwfParam(KafkaMessage):
    """This class contains the basic GWF file information
    
    """

    TOPIC_NAME = 'gwf_params'
    
    def __init__(self, channel: str = ""):
        """This class contains the basic GWF file information
        
        Keyword Arguments:

        """
        super(GwfParam, self).__init__(GwfParam.TOPIC_NAME)
        self.__channel = channel

    def __repr__(self):
        return str(self.__dict__)

    def __eq__(self, other):
        """Overrides the default implementation"""
        if isinstance(other, GwfParam):
            return self.__channel == other.__channel
        return False
