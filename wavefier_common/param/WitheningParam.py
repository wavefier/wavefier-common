from wavefier_common.kafka.Message import KafkaMessage
import uuid


class WitheningParam(object):
    """This class contains the basic Withening configuration that can be used to characterize the trigger
    """

    def __init__(self, arValue: int = 3000):
        """This class contains the basic Withening configuration that can be used to characterize the trigger
        
        Keyword Arguments:
            arValue {int} -- [The AR Value] (default: {3000})
        """
        self.uuid = str(uuid.uuid4())
        self.AR = arValue
        
    def __repr__(self):
        return str(self.__dict__)

    def __eq__(self, other):
        """Overrides the default implementation"""
        if isinstance(other, WitheningParam):
            return self.AR == other.AR
        return False

    def make_from_dic(self,dictionary):
        object=WitheningParam()
        for key in dictionary:
            setattr(object, key, dictionary[key])
        return object

