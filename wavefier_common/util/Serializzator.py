import jsonpickle
#import jsonpickle.ext.numpy as jsonpickle_numpy
#jsonpickle_numpy.register_handlers()

class Serializzator(object):

    @staticmethod
    def jsonEncode(obj:object) -> str :
        return jsonpickle.encode(obj)

    @staticmethod
    def jsonDencode(obj:str) -> object :
        return jsonpickle.decode(obj)
