from wavefier_common.util import Observer


class Observable(object):
    def __init__(self):
        self.obs = []
        self.changed = 0

    def addObserver(self, observer: Observer):
        """
        Add new Obeserver
        :param observer:
        :return:
        """
        if observer not in self.obs:
            self.obs.append(observer)

    def deleteObserver(self, observer: Observer):
        """
        Delete the Observer
        :param observer:
        :return:
        """
        self.obs.remove(observer)

    def notifyObservers(self, arg = None):
        '''If 'changed' indicates that this object
        has changed, notify all its observers, then
        call clearChanged(). Each observer has its
        update() called with two arguments: this
        observable object and the generic 'arg'.'''

        if not self.changed: return
        # Make a local copy in case of synchronous
        # additions of observers:
        localArray = self.obs[:]

        # Updating is not required to be synchronized:
        for observer in localArray:
            observer.update(self, arg)

    def deleteObservers(self): self.obs = []

    def setChanged(self): self.changed = 1

    def clearChanged(self): self.changed = 0

    def hasChanged(self): return self.changed

    def countObservers(self): return len(self.obs)
