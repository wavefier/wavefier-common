import pickle
import json
import copy

import os
import time


# 
class Parameters(object):
    """Class to handle paramenters
    
    """

    def __init__ ( self, **kwargs ):
        self.__dict__ = kwargs

        def __getattr__ ( self, attr ):
            return self.__dict__[attr]

    def dump ( self, filename ):
        """
        :param filename: name of file where saving the parameters
        :type filename: basestring
        """
        self.filename = filename
        with open(self.filename, mode='w', encoding='utf-8') as f:
            json.dump(self.__dict__, f)

    def load ( self, filename ):
        """
                :param filename: name of file where loading the parameters
                :type filename: basestring
                """
        self.filename = filename
        with open(self.filename) as data_file:
            data = json.load(data_file)
        self.__dict__ = data
        return self.__dict__

    def copy ( self, param ):
        """
                :param param: parameters
              """
        self.__dict__ = copy.deepcopy(param.__dict__)
        return self.__dict__

