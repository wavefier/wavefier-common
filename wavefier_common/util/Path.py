import pathlib
import inspect

class Path(type(pathlib.Path())):
    """Path Utils
    """

    @staticmethod
    
    def script_dir():
        """return script dir
        
        Returns:
            [type] -- [description]
        """

        return pathlib.Path(inspect.stack()[1].filename).parent.resolve()
