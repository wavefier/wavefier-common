import datetime


class Time(object):
    """Utility for the managemtn of time
    """

    @staticmethod
    def gps2UTCstr(gps: float) -> str:
        """GPS2UTCstr
        
        Arguments:
            gps {float} 
        
        Returns:
            str 
        """
        if isinstance(gps, str):
            gps = float()

        gps_epoch_as_gps = datetime.datetime(1980, 1, 6)
        # by definition
        gps_time_as_gps = gps_epoch_as_gps + datetime.timedelta(seconds=gps)
        gps_time_as_tai = gps_time_as_gps + datetime.timedelta(seconds=19)  # constant offset
        tai_epoch_as_tai = datetime.datetime(1970, 1, 1, 0, 0, 10)
        # by definition
        tai_timestamp = (gps_time_as_tai - tai_epoch_as_tai).total_seconds()
        utc = datetime.datetime.utcfromtimestamp(tai_timestamp)  # "right" timezone is in effect

        return str(utc) + ' UTC'

    @staticmethod
    def gps2isoformat(gps: float) -> str:
        """GPS2isoformat
        
        Arguments:
            gps {float} 
        
        Returns:
            str 
        """
        if isinstance(gps, str):
            gps = float()

        gps_epoch_as_gps = datetime.datetime(1980, 1, 6)
        # by definition
        gps_time_as_gps = gps_epoch_as_gps + datetime.timedelta(seconds=gps)
        gps_time_as_tai = gps_time_as_gps + datetime.timedelta(seconds=19)  # constant offset
        tai_epoch_as_tai = datetime.datetime(1970, 1, 1, 0, 0, 10)
        # by definition
        tai_timestamp = (gps_time_as_tai - tai_epoch_as_tai).total_seconds()
        utc = datetime.datetime.utcfromtimestamp(tai_timestamp)  # "right" timezone is in effect

        return str(utc.isoformat()) + "Z"


    @staticmethod
    def gps2epoctime(gps: float) -> float:
        if isinstance(gps, str):
            gps = float()

        gps_epoch_as_gps = datetime.datetime(1980, 1, 6)
        # by definition
        gps_time_as_gps = gps_epoch_as_gps + datetime.timedelta(seconds=gps)
        gps_time_as_tai = gps_time_as_gps + datetime.timedelta(seconds=19)  # constant offset
        tai_epoch_as_tai = datetime.datetime(1970, 1, 1, 0, 0, 10)
        # by definition
        tai_timestamp = (gps_time_as_tai - tai_epoch_as_tai).total_seconds()
        utc = datetime.datetime.utcfromtimestamp(tai_timestamp)  # "right" timezone is in effect
        return utc.timestamp()
