import logging

from confluent_kafka import Producer
from confluent_kafka.cimpl import KafkaException

from wavefier_common.kafka.Message import KafkaMessage


class KafkaProducer:
    """This class contain a Kafka Producer"""

    def __init__(self, conf, logger = None):
        """This class contains a Kafka Producer

                Keyword Arguments:
                    conf -- dictionary of configuration example: {'bootstrap.servers': broker}
        """
        self.logger = logger or logging.getLogger(__name__)
        self._producer = None
        try:
            # Producer configuration
            # See https://github.com/edenhill/librdkafka/blob/master/CONFIGURATION.md

            # Create Producer instance
            self._producer = Producer(**conf)
        except BufferError as ex:
            self.logger.error("Buffer error on creation of Producer", exc_info=True)

        except KafkaException as ex:
            self.logger.error("Custom Kafka error on creation of Producer", exc_info=True)

        except NotImplementedError as ex:
            self.logger.error('Exception while connecting Kafka', exc_info=True)


    # Optional per-message delivery callback (triggered by poll() or flush())
    # when a message has been successfully delivered or permanently
    # failed delivery (after retries).
    def delivery_callback(self, err, msg):
        if err:
            self.logger.error('%% Message failed delivery: %s\n' % err)
        else:
            self.logger.info('%% Message delivered to %s [%d] @ %o\n' %
                             (msg.topic(), msg.partition(), msg.offset()))

    def publish_message(self, message: KafkaMessage):
        """

        Keyword Arguments:
            message {KafkaMessage} -- [KafkaMessage to sent in a topic]
            topic {str} -- [Destination topic]
        """
        topic = message.get_topic()
        return self.publish_message_on_topic(topic, message)

    def publish_message_on_topic(self, topic:str, message: KafkaMessage):
        """

        Keyword Arguments:
            topic {str} -- Force Kafka topic
            message {KafkaMessage} -- [KafkaMessage to sent in a topic]
            topic {str} -- [Destination topic]
        """
        try:
            # Produce line (without newline)
            self._producer.produce(topic, message.serialize(), on_delivery=self.delivery_callback)
        except BufferError as ex:
            self.logger.exception("Buffer Error on produce message")
            return ex

        try:

            # Serve delivery callback queue.
            # NOTE: Since produce() is an asynchronous API this poll() call
            #       will most likely not serve the delivery callback for the
            #       last produce()d message.
            self._producer.poll(0)
            self._producer.flush()
        except Exception as ex:
            self.logger.exception('Exception in publishing message')
            return ex
