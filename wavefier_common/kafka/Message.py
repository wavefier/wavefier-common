
from wavefier_common.util.Serializzator import Serializzator


class KafkaMessage:
    """This class contain a Message sent to Kafka"""

    def __init__(self, topic: str = 'test'):
        self.__k_topic = topic

    def get_topic(self):
        """Get message topic

        Returns:
            str -- [the topic name for the specific message]
        """
        return self.__k_topic

    def serialize(self):
        """Get message serialized

        Returns:
            bytes -- [the value of the message serialized in bytes]
        """
        return bytes(Serializzator.jsonEncode(self), 'utf-8')

    def deserialize(self, raw_value):
        """Get message deserialized

        Returns:
            str -- [the value of message deserialized]
        """
        return Serializzator.jsonDencode(raw_value.decode('utf-8'))
