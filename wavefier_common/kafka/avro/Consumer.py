import logging

from confluent_kafka.avro import AvroConsumer
from confluent_kafka.avro.serializer import SerializerError
from confluent_kafka.cimpl import KafkaError

from wavefier_common.kafka.avro.Message import Message
from wavefier_common.kafka.avro.TimeOutException import TimeOutException

class Consumer:
    """This class contains a Kafka Avro Consumer"""

    def __init__(self, conf, logger=None):

        self.logger = logger or logging.getLogger(__name__)
        self._consumer = None
        self._message = None
        try:
            self._consumer = AvroConsumer(conf)
        except ValueError as ex:
            self.logger.error("Error on Consumer creation", exc_info=True)
            return ex

    def subscribe(self, msg: Message):
        self._message = msg
        topic = self._message.get_topic()
        self.logger.info("Set  topic to: {}".format(topic))

        self._consumer.subscribe([topic])

    def consume_message(self, timeout_in_seconds=None):

        """
            Consume a message from subscribed topics
        """
        eof_reached = dict()

        while True:
            try:
                if timeout_in_seconds is None:
                    msg = self._consumer.poll(30.0)
                else:
                    msg = self._consumer.poll(timeout_in_seconds)

                # There were no messages on the queue, continue polling
                if msg is None:
                    raise TimeOutException('Got timeout from poll() without a timeout set: %s' % timeout_in_seconds)

                if msg.error():
                    if msg.error().code() == KafkaError._PARTITION_EOF:
                        self.logger.warning('Reached end of %s [%d] at offset %d' %
                                            (msg.topic(), msg.partition(), msg.offset()))
                        eof_reached[(msg.topic(), msg.partition())] = True
                        if len(eof_reached) == len(self._consumer.assignment()):
                            self.logger.warning('EOF reached for all assigned partitions: exiting')
                            break
                    else:
                        self.logger.warning('Consumer error: %s: ignoring' % msg.error())
                        break

                objCreated = self._message.object_from_dic(msg.value())
                
                return objCreated

            except SerializerError as e:
                # Report malformed record, discard results, continue polling
                self.logger.warning("Message deserialization failed {}".format(e))
                break

