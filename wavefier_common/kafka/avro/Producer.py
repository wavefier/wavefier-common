import logging

from confluent_kafka import avro
from confluent_kafka.avro import AvroProducer, SerializerError
from confluent_kafka import KafkaException
from wavefier_common.kafka.avro.Message import Message



class Producer(object):
    """This class contain a Kafka Producer"""

    def __init__(self, conf, logger = None):
        """This class contains a Kafka Producer

                Keyword Arguments:
                    conf -- dictionary of configuration example: {'bootstrap.servers': broker}
        """
        self.logger = logger or logging.getLogger(__name__)
        self._producer = None

        if ("schema.registry.url" in conf) or ("bootstrap.servers" in conf) :
            try:
                self._producer = AvroProducer(conf)

            except BufferError as ex:
                self.logger.error("Buffer error on creation of Producer", exc_info=True)

            except KafkaException as ex:
                self.logger.error("Custom Kafka error on creation of Producer", exc_info=True)
                raise Exception('Exception while connecting Kafka')  from ex


            except SerializerError as ex:
                self.logger.error('Exception while connecting Kafka', exc_info=True)
                raise Exception('Exception while connecting Kafka')  from ex

        else:
            errmsg='Missing importat configuration, please refere https://docs.confluent.io/current/clients/confluent-kafka-python/#confluent_kafka.avro.AvroProducer'
            self.logger.error(errmsg, exc_info=True)
            raise Exception(errmsg)

        self.logger.info('Producer is Ready')



    def default_delivery_callback(self, err, msg, obj):
        """
            Handle delivery reports served from producer.poll.
            This callback takes an extra argument, obj.
            This allows the original contents to be included for debugging purposes.
        """
        if err is not None:
            self.logger.error('Message {} delivery failed with error {}'.format(
                    str(obj), err
                )
            )
        else:
            self.logger.info('Message successfully produced to {} [{}] at offset {}. Message: {} '.format(
                     msg.topic(), msg.partition(), msg.offset(), str(obj)
                )
            )

    def sync_delivery(self, message: Message ):
        self.delivery(message, self.default_delivery_callback)
        try:

            # Serve delivery callback queue.
            # NOTE: Since produce() is an asynchronous API this poll() call
            #       will most likely not serve the delivery callback for the
            #       last produce()d message.
            self._producer.poll(0)
            self._producer.flush()
            return True
        except Exception as ex:
            self.logger.exception('Exception in publishing message')
            return ex

    """
        UN-TESTED: THE CALLBACK DOESN'T WORK IF PASSED AS PARAMETER
    """
    def delivery(self, message: Message, on_delivery_callbak=None):

        if not isinstance(message, Message):
            self.logger.error("Invalid Message input, discarding record")
        else:
            self.logger.info("OK New message to manage")

        if on_delivery_callbak is None:
            self.logger.info("Set default deliveri message")
            on_delivery=self.default_delivery_callback
        else:
            on_delivery=on_delivery_callbak

        try:
            self._producer.produce(
               topic=message.get_topic(),
               value=message.get_value(Message.FIELD_FORMAT.DICT),
               value_schema=message.get_value_schema(),
               key=message.get_key(Message.FIELD_FORMAT.DICT),
               key_schema=message.get_key_schema(),
               on_delivery=lambda err, msg, obj=message.get_value(): on_delivery(err, msg, obj)
            )

        except ValueError as ex:
            self.logger.exception("Invalid input, discarding record...")
            return ex


