import inspect
from pathlib import Path
from enum import Enum
from confluent_kafka import avro


class Message:
    """This class contain a Message sent to Kafka"""

    class FIELD_FORMAT(Enum):
        ORIGINAL = 0
        DICT = 1

    def __init__(self, topic: str = 'test', object: object = None, force_object_schema: str = None, key: object = None,
                 force_key_schema: str = None):

        if object is None:
            raise ValueError("Inappropriate argument value object (of correct type)")

        if "make_from_dic" not in dir(object):
                raise ValueError("The object that you passed need to have a function called make_from_dic(object). This function create the current object from dictionary")

        if topic is None:
            raise ValueError("Inappropriate argument value topic (of correct type)")

        """ Check is a class https://stackoverflow.com/questions/14549405/python-check-instances-of-classes """
        if not hasattr(object, '__class__'):
            raise ValueError("Object not have a type object ")

        if key is not None and not hasattr(key, '__class__'):
            raise ValueError("Object not have a type object ")

        self.topic = topic

        self.key = key

        self.value = object

        if force_key_schema is None and key is not None:
            self.key_schema = Message.load_avro_schema(key)
        else:
            self.key_schema = force_key_schema

        if force_object_schema is None and object is not None:
            self.value_schema = Message.load_avro_schema(object)
        else:
            self.value_schema = force_object_schema

    @staticmethod
    def load_avro_schema(object: object):
        fullfilepath = Path(inspect.getfile(object.__class__))
        class_filename = fullfilepath.stem
        class_filepath = fullfilepath.parent
        avroschema_filepath = class_filepath.joinpath(class_filename + ".avsc")
        return avro.load(avroschema_filepath)

    @staticmethod
    def dic_from_object(object: object):
        _excluded_keys = set(object.__class__.__dict__.keys())
        outdic={}
        for (key, value) in object.__dict__.items():
            if key not in _excluded_keys :
                outdic.update({ key : value})
        return outdic

    def object_from_dic(self,dictionary={}):
        value = self.get_value()
        if "make_from_dic" not in dir(value):
            raise GeneratorExit("Object do not have make_from_dic  function! Aborted the serialization.")
        else:
            return value.make_from_dic(dictionary)

    def get_key(self, format: FIELD_FORMAT = FIELD_FORMAT.ORIGINAL):
        if self.key is not None and Message.FIELD_FORMAT.DICT == format :
            return Message.dic_from_object(self.key)
        else:
            return self.key

    def get_key_schema(self):
        return self.key_schema

    def get_value(self, format: FIELD_FORMAT = FIELD_FORMAT.ORIGINAL):
        if self.value is not None and Message.FIELD_FORMAT.DICT == format :
            return Message.dic_from_object(self.value)
        else:
            return self.value

    def get_value_schema(self):
        return self.value_schema

    def get_topic(self):
        return self.topic


    def __eq__(self, other):
        """Overrides the default implementation"""
        checkA = False
        if isinstance(other, Message):
            checkA = (self.topic == other.topic \
                     and self.key == other.key \
                     and self.value == other.value \
                     and self.key_schema == other.key_schema \
                     and self.value_schema == other.value_schema )
        return checkA