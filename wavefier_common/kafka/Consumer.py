import logging

from confluent_kafka import Consumer
from confluent_kafka.cimpl import KafkaError

from wavefier_common.kafka.Message import KafkaMessage


class KafkaConsumer:
    """This class contains a Kafka Consumer"""

    def __init__(self, conf, logger=None):
        """This class contains a Kafka Consumer

                Keyword Arguments:
                    conf -- dictionary of configuration
                        example:
                            {
                                'bootstrap.servers': broker,
                                'group.id': 'mygroup',
                                'default.topic.config': {
                                    'auto.offset.reset': 'smallest'
                                }
                            }
        """
        self.logger = logger or logging.getLogger(__name__)
        try:
            self._consumer = Consumer(**conf)
            self._message = KafkaMessage()
        except Exception as ex:
            self.logger.error("Error on Consumer creation", exc_info=True)
            return ex

    def subscribe(self, msg: KafkaMessage):
        self._message = msg
        """Subscribe to a list of topic

         Keyword Arguments:
            topic -- [List of topic to subscribe]
        """

        topic = self._message.get_topic()
        self._consumer.subscribe([topic])

    def consume_message(self, timeout_in_seconds=None):
        """Consume a message from subscribed topics
        """
        eof_reached = dict()
        while True:
            if timeout_in_seconds is None:
                msg = self._consumer.poll()
            else:
                msg = self._consumer.poll(timeout_in_seconds)

            if msg is None:
                raise Exception('Got timeout from poll() without a timeout set: %s' % msg)

            if msg.error():
                if msg.error().code() == KafkaError._PARTITION_EOF:
                    self.logger.warning('Reached end of %s [%d] at offset %d' %
                          (msg.topic(), msg.partition(), msg.offset()))
                    eof_reached[(msg.topic(), msg.partition())] = True
                    if len(eof_reached) == len(self._consumer.assignment()):
                        self.logger.warning('EOF reached for all assigned partitions: exiting')
                        break
                else:
                    self.logger.warning('Consumer error: %s: ignoring' % msg.error())
                    break

            return self._message.deserialize(msg.value())
