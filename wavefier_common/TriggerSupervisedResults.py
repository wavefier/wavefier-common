import json

class TriggerSupervisedResults(object):
    """
    This class contains the classification information about the Trigger: configurations of algorithms used and the
    results of analysis
    """
    def __init__(self, uuid: str=None, algorithm_config: dict={}, ml_results: dict={}):
        """
        This class contains the classification information about the Trigger: configurations of algorithms used and the
        results of analysis

        :type uuid: uuid.uuid4
        :param uuid: Unique id of the Trigger
        """
        self.uuid = uuid
        self.algorithm_config = algorithm_config
        self.ml_results = ml_results


    def getUUID(self):
        """
        Get UUID of the analysed Trigger

        :return: UUID of the Trigger
        """
        return self.uuid

    def getAlgorithmConfig(self, algorithm_name: str):
        """
        Get the configuration of algorithm, specified by name, used in the classification of the Trigger

        :type algorithm_name: str
        :param algorithm_name: Name of the algorithm used to classify the Trigger
        :return: The configuration of the algorithm used to classify the Trigger
        """
        return self.algorithm_config[algorithm_name]

    def getMLResults(self, algorithm_name: str):
        """
        Get the results of particular classification, specified by name, used in the classification of the Trigger

        :type algorithm_name: str
        :param algorithm_name: Name of the algorithm used to classify the Trigger
        :return: The results of the classification of the Trigger
        """
        return self.ml_results[algorithm_name]

    def setAlgorithmConfig(self, algorithm_name: str, algorithm_config: dict):
        """
        Set the configuration of algorithm, specified by name, used in the classification of the Trigger

        :type algorithm_name: str
        :param algorithm_name: Name of the algorithm used to classify the Trigger

        :type algorithm_config: dict
        :param algorithm_config: The configuration of the algorithm used to classify the Trigger
        """
        string_config = json.dumps(algorithm_config)
        self.algorithm_config[algorithm_name] = string_config

    def setMLResults(self, algorithm_name: str, ml_results: int):
        """
        Set the results of ML analysis, specified by name, used in the classification of the Trigger

        :type algorithm_name: str
        :param algorithm_name: Name of the algorithm used to classify the Trigger

        :type ml_results: int
        :param ml_results: The results of the classification of the Trigger
        """
        self.ml_results[algorithm_name] = ml_results

    def __repr__(self):
        return str(self.__dict__)

    def __eq__(self, other):
        """Overrides the default implementation"""
        if isinstance(other, TriggerSupervisedResults):
            return self.uuid == other.uuid \
                and self.algorithm_config == other.algorithm_config \
                and self.ml_results == other.ml_results
        return False

    def make_from_dic(self,dictionary):
        object=TriggerSupervisedResults()
        for key in dictionary:
            setattr(object, key, dictionary[key])

        return object
