from wavefier_common.kafka.Message import KafkaMessage


class RawData(KafkaMessage):
    """This class contains the RawData information

    """

    TOPIC_NAME = 'raw_data'

    def __init__(self, filename: str, data):
        """This class contains the RawData information

        Keyword Arguments:
            filename {str} -- [filename read]
            data -- [data stored in file read]
        """
        super(RawData, self).__init__(RawData.TOPIC_NAME)
        self.__filename = str(filename)
        self.__startGpsTime = float(self.__filename.split("-")[-1].split(".")[0])
        self.__data = data

    def getData(self):
        """The data stored in file read

        Returns:
            the data stored in file read
        """
        return self.__data

    def getFilename(self):
        """The filename read

        Returns:
            str -- the filename
        """
        return self.__filename

    def getStartGPSTime(self):
        # TODO: we accept this in current version,maybe in the next release is better retrieve this info in an other way

        """The Gps start time of data

        Returns:
            float -- the gps time
        """
        return self.__startGpsTime

    def __repr__(self):
        return self.__filename


