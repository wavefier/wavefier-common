Welcome to wavefier_common's documentation!
====================================================
The wavefier_common library is part of the Wavefier project. This module contains all the definition of data structure used in the entire wavefier system.


Table of content
----------------

.. toctree::
   :maxdepth: 2

   structure/introduction
   structure/apidoc

Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`