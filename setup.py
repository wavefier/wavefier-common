#!/usr/bin/env python

import wavefier_common

from setuptools import setup, find_packages

setup(
    name='wavefier_common',
    author=wavefier_common.__author__,
    version=wavefier_common.__version__,
    description='This library has the intent to contain all the common part used in the wavefier project.',
    url='https://gitlab.trust-itservices.com/wavefier/wavefier-common',
    author_email='',
    packages=find_packages(exclude=['bin', 'data', 'docs', 'tests', 'tools']),
    package_data={
        'wavefier_common': ['*'],
        'wavefier_common.param': ['*']
    },
    long_description=open('README.md').read(),
    setup_requires=['nose>=1.0'],
    install_requires=[
        'confluent-kafka[avro]==1.0',
        'jsonpickle'
    ],
    test_suite='nose.collector'
)

