import os

import shutil

from wavefier_common.util.Path import Path
from wavefier_common.param.WdfParam import WdfParam
from wavefier_common.param.WitheningParam import WitheningParam

from unittest import TestCase
from wavefier_common.param.WitheningParam import WitheningParam
from wavefier_common.util.Serializzator import Serializzator


class SerializatorTest(TestCase):

    def test_Simple(self):
        tmp=WitheningParam('a')

        frozen=Serializzator.jsonEncode(tmp)

        tmp2=Serializzator.jsonDencode(frozen)

        self.assertEqual(tmp.__dict__,tmp2.__dict__)
        