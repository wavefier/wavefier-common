import os
from unittest import TestCase

from wavefier_common.RawData import RawData
from wavefier_common.util.Path import Path


class RawDataTest(TestCase):

    def test_creation(self):
        script_dir = os.path.abspath(os.path.dirname(__file__))
        subdir = os.path.join(script_dir, 'test_static_data')

        file_path = str(Path(subdir)/'V-aaa-1222593214.gwf')

        f = open(file_path, "rb")
        message = RawData("V-aaa-1222593214.gwf", f.read())
        self.assertEqual(1222593214, message.getStartGPSTime())

        self.assertEqual("V-aaa-1222593214.gwf", message.getFilename())




