from unittest import TestCase
import tests

from wavefier_common.Trigger import Trigger

from wavefier_common.kafka.avro.Producer import Producer
from wavefier_common.kafka.avro.Consumer import Consumer
from wavefier_common.kafka.avro.Message import Message
from wavefier_common.kafka.avro.TimeOutException import TimeOutException
from wavefier_common.param.WdfParam import WdfParam
from wavefier_common.param.WitheningParam import WitheningParam


class TestAvroConsumer(TestCase):

    def test_creation_ko1(self):
        try:
            consumer = Consumer()
            self.fail()
        except Exception as ex:
            self.assertTrue(True)

    def test_creation_ko2(self):
        try:
            consumer = Consumer({'bootstrap.servers': tests.kafka_broker})
            self.fail()
        except Exception as ex:
            self.assertTrue(True)

    def test_creation_ok(self):

        conf = {'bootstrap.servers': tests.kafka_broker,
                'schema.registry.url': tests.kafka_schema_registry,
                'group.id': tests.generate_group_id()
                }

        try:
            consumer = Consumer(conf)
            self.assertIsNotNone(consumer)
        except Exception as ex:
            self.fail()

    def test_consume_Trigger(self):
        conf = {'bootstrap.servers': tests.kafka_broker,
                'schema.registry.url': tests.kafka_schema_registry}

        producer = Producer(conf)

        objectOriginal = Trigger(
            'a', 'b', 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 'alex', 2, 1)
        objectOriginal.setRW(0, 1.0)
        objectOriginal.setWT(1, 2.0)

        message = Message('test_avro_topic', object=objectOriginal)
        responce = producer.sync_delivery(message)
        self.assertTrue(responce)

        conf = {'bootstrap.servers': tests.kafka_broker,
                'schema.registry.url': tests.kafka_schema_registry,
                'group.id': tests.generate_group_id(),
                'default.topic.config': {
                    'auto.offset.reset': 'smallest'
                }
                }

        consumer = Consumer(conf)
        consumer.subscribe(message)

        objectConsumed = consumer.consume_message(30)

        while objectConsumed.uuid != objectOriginal.uuid:
            objectConsumed = consumer.consume_message(30)
        self.assertEqual(objectOriginal, objectConsumed)

    def test_consume_WDFParam(self):
        conf = {'bootstrap.servers': tests.kafka_broker,
                'schema.registry.url': tests.kafka_schema_registry}

        producer = Producer(conf)

        objectOriginal = WdfParam(12, 1)

        message = Message('test_WdfParam_topic', object=objectOriginal)
        responce = producer.sync_delivery(message)
        self.assertTrue(responce)

        conf = {'bootstrap.servers': tests.kafka_broker,
                'schema.registry.url': tests.kafka_schema_registry,
                'group.id': tests.generate_group_id(),
                'default.topic.config': {
                    'auto.offset.reset': 'smallest'
                }
                }

        consumer = Consumer(conf)
        consumer.subscribe(message)

        objectConsumed = consumer.consume_message(30)

        while objectConsumed.uuid != objectOriginal.uuid:
            objectConsumed = consumer.consume_message(30)
        self.assertEqual(objectOriginal, objectConsumed)

    def test_timeout(self):

        objectOriginal = WitheningParam(12)

        message = Message('test_WitheningParam_topic', object=objectOriginal)

        conf = {
            'bootstrap.servers': tests.kafka_broker,
            'schema.registry.url': tests.kafka_schema_registry,
            'group.id': tests.generate_group_id(),
            'default.topic.config': {
                'auto.offset.reset': 'smallest'
            }
        }

        consumer = Consumer(conf)
        consumer.subscribe(message)

        try:
            objectConsumed = consumer.consume_message(2)
            self.fail("NOT BE THERE!")
        except TimeOutException as identifier:
            pass

    def test_consume_WitheningParam(self):

        conf = {
            'bootstrap.servers': tests.kafka_broker,
            'schema.registry.url': tests.kafka_schema_registry
        }

        producer = Producer(conf)

        objectOriginal = WitheningParam(12)

        message = Message('test_WitheningParam_topic', object=objectOriginal)
        responce = producer.sync_delivery(message)
        self.assertTrue(responce)

        conf = {'bootstrap.servers': tests.kafka_broker,
                'schema.registry.url': tests.kafka_schema_registry,
                'group.id': tests.generate_group_id(),
                'default.topic.config': {
                    'auto.offset.reset': 'smallest'
                }
                }

        consumer = Consumer(conf)
        consumer.subscribe(message)

        objectConsumed = consumer.consume_message(30)

        while objectConsumed.uuid != objectOriginal.uuid:
            objectConsumed = consumer.consume_message(30)
        self.assertEqual(objectOriginal, objectConsumed)