import os
from unittest import TestCase
import copy
import shutil

from wavefier_common.Trigger import Trigger

from wavefier_common.TriggerCollection import TriggerCollection
from wavefier_common.param.WdfParam import WdfParam
from wavefier_common.param.WitheningParam import WitheningParam

class TriggerCollectionTest(TestCase):

    def test_Creation(self):
        tmp = TriggerCollection(  wthParam= WitheningParam(), wdfParam= WdfParam())
        self.assertEqual(tmp.getChannel(),"unknown")
        self.assertEqual(tmp.getGPSSegment(),0.0)
        self.assertEqual(tmp.getWTHParam('AR'),WitheningParam().AR)
        self.assertEqual(tmp.getWDFParam('overlap'),WdfParam().overlap)
        self.assertEqual(tmp.count(),0)

    def test_add(self):
        tmp = TriggerCollection(  wthParam= WitheningParam(), wdfParam= WdfParam())
        trigger=Trigger('a','b')

        self.assertEqual(tmp.count(),0)
        
        tmp.add(trigger)
        self.assertEqual(tmp.count(),1)
       
        mIterator=tmp.iterator()
        self.assertEqual(trigger,next(mIterator))
            
    def test_adds(self):
        tmp = TriggerCollection(wthParam= WitheningParam(), wdfParam= WdfParam())
        triggers=[Trigger('a','b'),Trigger('a','b')]

        self.assertEqual(tmp.count(),0)
        
        tmp.adds(triggers)
        self.assertEqual(tmp.count(),2)

    def test_equals(self):
        tmp = TriggerCollection(wthParam= WitheningParam(), wdfParam= WdfParam())
        triggers = [Trigger('a','b'), Trigger('a','b')]
        tmp.adds(triggers)

        #tmp1 = copy.deepcopy(tmp)
        #triggers = [Trigger('a','b'), Trigger('a','b')]
        #tmp1.adds(triggers)
        #self.assertEqual(tmp, tmp1)