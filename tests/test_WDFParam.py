from unittest import TestCase

from wavefier_common.param.WdfParam import WdfParam


class WdfParamTest(TestCase):


    def test_creation_default(self):
        tmp=WdfParam()
        self.assertEqual(tmp.threshold,2.0)
        self.assertEqual(tmp.overlap,16)
        self.assertEqual(tmp.window,1024)
    
    def test_creation(self):
        tmp=WdfParam(512,512,512.0)
        self.assertEqual(tmp.threshold,512.0)
        self.assertEqual(tmp.overlap,512)
        self.assertEqual(tmp.window,512)