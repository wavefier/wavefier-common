import os
from unittest import TestCase

import shutil

from wavefier_common.util.Time import Time

class TimeTest(TestCase):
    
    def test_utc_str(self):
        expected="2018-10-12 20:41:22 UTC"
        time = 1223412073
        actual= Time.gps2UTCstr(time)
        self.assertEqual(expected, actual)

    def test_utc_str_2(self):
        expected="2018-10-12 20:41:23.035156 UTC"
        time = 1223412074.0351562
        actual= Time.gps2UTCstr(time)
        self.assertEqual(expected, actual)

    def test_isoformat_str(self):
        expected="2018-10-12T20:41:22Z"
        time = 1223412073
        actual= Time.gps2isoformat(time)
        self.assertEqual(expected, actual)
    
    def test_isoformat_str_2(self):
        expected="2018-10-12T20:41:23.035156Z"
        time = 1223412074.0351562
        actual= Time.gps2isoformat(time)
        self.assertEqual(expected, actual)


    def test_epoctime_str_2(self):
        expected = 1539373283.035156
        #expected="2018-10-12T20:41:23.035156Z"
        time = 1223412074.0351562
        actual= Time.gps2epoctime(time)
        errore = abs(expected - actual)
        self.assertTrue(errore < 4000.0)