import os
from unittest import TestCase

import shutil

from wavefier_common.util.Path import Path


class PathTest(TestCase):

    def test_script_dir(self):
        expected = os.path.abspath(os.path.dirname(__file__))
        actual = str(Path.script_dir())
        self.assertEqual(expected, actual)

    def test_file_access(self):
        script_dir = os.path.abspath(os.path.dirname(__file__))
        subdir = os.path.join(script_dir, 'test_data')
        if Path(subdir).is_dir():
            shutil.rmtree(subdir)
        os.makedirs(subdir)
        file_path = str(Path(subdir)/'file.txt')
        content = '123'
        tmp=open(file_path, 'w')
        tmp.write(content)
        tmp.close()

        test_path = Path.script_dir()/subdir/'file.txt'
        tmp2 = open(str(test_path))
        actual = tmp2.read()
        tmp2.close()
        self.assertEqual(content, actual)



