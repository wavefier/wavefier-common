import os
from unittest import TestCase

import shutil

from wavefier_common.util.Path import Path
from wavefier_common.util.Parameters import Parameters


class ParametersTest(TestCase):
    def test_load_file(self):
        script_dir = os.path.abspath(os.path.dirname(__file__))
        subdir = os.path.join(script_dir, 'test_static_data')

        file_path = str(Path(subdir)/'test_parameters.json')
        config = Parameters()
        config.load(file_path)
        expected = "ok"
        actual = config.is_it_load
        self.assertEqual(expected, actual)
