
from unittest import TestCase
import tests

from wavefier_common.kafka.Consumer import KafkaConsumer
from wavefier_common.kafka.Message import KafkaMessage
from wavefier_common.kafka.Producer import KafkaProducer
from wavefier_common.param.WdfParam import WdfParam
from wavefier_common.param.WitheningParam import WitheningParam

class MessageA(KafkaMessage):

    def __init__(self ):
        super().__init__('CLASSIC_TOPIC_TEST')
        self.A1=23
        self.A2=25

    def __eq__(self, other: object) -> bool:
        if isinstance(other, MessageA):
            return self.A1 == other.A1 \
                     and self.A2 == other.A2
        return False

class MessageB(KafkaMessage):

    def __init__(self):
        super().__init__('CLASSIC_TOPIC_TEST')
        self.B1=23
        self.B2=25

    def __eq__(self, other: object) -> bool:
        if isinstance(other, MessageB):
            return self.B1 == other.B1 \
                     and self.B2 == other.B2
        return False


class ClusterTest(TestCase):

    def test_creation(self):

        message = MessageA()
        message_wdf = MessageB()

        producer = KafkaProducer({'bootstrap.servers': tests.kafka_broker })
        producer.publish_message(message)
        producer.publish_message(message_wdf)

        consumer = KafkaConsumer({
            'bootstrap.servers': tests.kafka_broker,
            'group.id': tests.generate_group_id(),
            'default.topic.config': {
                'auto.offset.reset': 'smallest'
            }
        })

        consumer.subscribe(message)
        msg_consumed = consumer.consume_message()

        consumer.subscribe(message_wdf)
        msg_consumed_wdf = consumer.consume_message()

        self.assertEqual(message, msg_consumed)
        self.assertEqual(message_wdf, msg_consumed_wdf)
