from unittest import TestCase

from wavefier_common.TriggerSupervisedResults import TriggerSupervisedResults
from wavefier_common.kafka.avro.Producer import Producer
from wavefier_common.kafka.avro.Consumer import Consumer
from wavefier_common.kafka.avro.Message import Message

import os
import tests


class TestTriggerSupervisedResults(TestCase):


    def test_produce_results(self):
       

        # Connecting to the kafka broker
        conf = {'bootstrap.servers': tests.kafka_broker,
                'schema.registry.url': tests.kafka_schema_registry,
                'group.id': tests.generate_group_id()}

        producer = Producer(conf)

        objectOriginal = TriggerSupervisedResults("042b79ed-c4cc-48ed-910d-18174d9ae31d")
        objectOriginal.setAlgorithmConfig("cnn1d", {
            "loss": {"optimizer": "Adam", "lr": 0.0001, "cost": "categorical_crossentropy"}, "architecture": {
                "network": {"dense": [[512, "relu", 0.2], [256, "relu", 0.2]],
                            "convolution": [[40, 5, "relu", 0.2, 2], [20, 5, "relu", 0.2, 2]]}},
            "hyper_params": {"max_iter": 5, "batch_size": 128, "split_data": 20, "val_ratio": 0.2, "n_classes": 5}})
        objectOriginal.setMLResults("cnn1d", 0)

        message = Message('test_TriggerSupervisedResults_topic', object=objectOriginal)
        responce = producer.sync_delivery(message)
        self.assertTrue(responce)

    def test_consume_results(self):


        # Connecting to the kafka broker
        conf = {'bootstrap.servers': tests.kafka_broker,
                'schema.registry.url': tests.kafka_schema_registry,
                'group.id': tests.generate_group_id()}

        producer = Producer(conf)

        objectOriginal = TriggerSupervisedResults("042b79ed-c4cc-48ed-910d-18174d9ae31d")
        objectOriginal.setAlgorithmConfig("cnn1d", {
            "loss": {"optimizer": "Adam", "lr": 0.0001, "cost": "categorical_crossentropy"}, "architecture": {
                "network": {"dense": [[512, "relu", 0.2], [256, "relu", 0.2]],
                            "convolution": [[40, 5, "relu", 0.2, 2], [20, 5, "relu", 0.2, 2]]}},
            "hyper_params": {"max_iter": 5, "batch_size": 128, "split_data": 20, "val_ratio": 0.2, "n_classes": 5}})
        objectOriginal.setMLResults("cnn1d", 0)

        message = Message('test_TriggerSupervisedResults_topic', object=objectOriginal)
        responce = producer.sync_delivery(message)
        self.assertTrue(responce)

        conf = {'bootstrap.servers': tests.kafka_broker,
                'schema.registry.url': tests.kafka_schema_registry,
                'group.id': tests.generate_group_id(),
                'default.topic.config': {
                    'auto.offset.reset': 'smallest'
                }
                }

        consumer = Consumer(conf)
        consumer.subscribe(message)

        objectConsumed = consumer.consume_message(30)
        while objectConsumed.uuid != objectOriginal.uuid:
            objectConsumed = consumer.consume_message(30)
        self.assertEqual(objectOriginal, objectConsumed)
