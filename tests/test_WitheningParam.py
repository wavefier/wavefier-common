from unittest import TestCase

from wavefier_common.param.WitheningParam import WitheningParam


class WitheningParamTest(TestCase):

    def test_creation_default(self):
        tmp=WitheningParam()
        self.assertEqual(tmp.AR, 3000)

    def test_creation(self):
        tmp=WitheningParam(512)
        self.assertEqual(tmp.AR, 512)
