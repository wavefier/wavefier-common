from unittest import TestCase
import tests

from wavefier_common.Trigger import Trigger

from confluent_kafka import avro
from wavefier_common.util.Path import Path

from wavefier_common.kafka.avro.Producer import Producer
from wavefier_common.kafka.avro.Message import Message


class TestAvroProducer(TestCase):

    def on_dummy_delivery(self, err, msg, obj):
        print(msg)
        print(obj)
        print(err)

        self.fail()

        if err is not None:
            self.assertFalse(False)
        else:
            self.assertTrue(True)

    def test_creation_ko1(self):
        try:
            producer = Producer()
            self.fail()
        except Exception as ex:
            self.assertTrue(True)

    def test_creation_ko2(self):
        try:
            producer = Producer({'bootstrap.servers': tests.kafka_broker})
            self.fail()
        except Exception as ex:
            self.assertTrue(True)

    def test_creation_ok(self):

        conf = {'bootstrap.servers': tests.kafka_broker,
                'schema.registry.url': tests.kafka_schema_registry}

        try:
            producer = Producer(conf)
            self.assertIsNotNone(producer)
        except Exception as ex:
            self.fail()

    def test_delivery(self):
        conf = {'bootstrap.servers': tests.kafka_broker,
                'schema.registry.url': tests.kafka_schema_registry
                }

        producer = Producer(conf)
        message = Message('test_avro_topic', object=Trigger('a', 'b'))
        producer.delivery(message, on_delivery_callbak=lambda a,
                          b, c: self.on_dummy_delivery(a, b, c))

    def test_sync_delivery(self):
        conf = {'bootstrap.servers': tests.kafka_broker,
                'schema.registry.url': tests.kafka_schema_registry}

        producer = Producer(conf)
        message = Message('test_avro_topic', object=Trigger('a', 'b'))
        responce = producer.sync_delivery(message)
        self.assertTrue(responce)
