from wavefier_common.kafka.avro.Producer import Producer
from wavefier_common.kafka.avro.Consumer import Consumer
from wavefier_common.kafka.avro.Message import Message
from wavefier_common.param.WdfParam import WdfParam

import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

if __name__ == "__main__":
    conf = {'bootstrap.servers': "kafka_test:9092",
            'schema.registry.url': "http://kafka-schema-registry_test:8081"}

    producer = Producer(conf)

    objectOriginal = WdfParam(window=1024, overlap=16, threshold = 2.0,
                 BLfilterOrder = 9, ResamplingFactor = 4, learn =300)


    logging.info(objectOriginal)

    message = Message('test_WdfParam_topic', object=objectOriginal)
    responce = producer.sync_delivery(message)
    logging.info(responce)

    conf = {'bootstrap.servers': "kafka_test:9092",
            'schema.registry.url': "http://kafka-schema-registry_test:8081",
            'group.id': 'testGroups',
            'default.topic.config': {
                'auto.offset.reset': 'smallest'
            }
            }

    consumer = Consumer(conf)
    consumer.subscribe(message)

    objectConsumed = consumer.consume_message(30)
    logging.info(objectConsumed)
    while objectConsumed.uuid != objectOriginal.uuid:
        objectConsumed = consumer.consume_message(30)
        logging.info(objectConsumed)
