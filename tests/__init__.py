import random

broker_host = "kafka"
broker_port = 9092
registry_host = "http://schema_registry"
registry_port = 8081
channel = "unit_test01"
host_db = "influxdb"
port_db = 8086
db_name = "trigger_report_db"

kafka_broker = broker_host + ":" + str(broker_port)
kafka_schema_registry = registry_host + ":" + str(registry_port)


def generate_group_id():
    return 'testGroups' + str(random.random())
