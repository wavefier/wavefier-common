import os
from unittest import TestCase
import copy

from wavefier_common.Trigger import Trigger


class TriggerTest(TestCase):

    def test_init(self):
        tmp = Trigger('a','b')
        self.assertEqual(1, tmp.getPartition())

    def test_default_1(self):
        expected = 0.2
        tmp = Trigger('a','b',expected)
        actual = tmp.getGPS()
        self.assertEqual(expected, actual)

    def test_default_2(self):
        tmp = Trigger('a','b',0.2, 21)

        self.assertEqual(tmp.getGPS(), 0.2)
        self.assertEqual(tmp.getSNR(), 21)

    def test_dics_1(self):
        tmp = Trigger('a','b')
        tmp.setWT(2,2)
        self.assertEqual(tmp.getWT(2),2.0)

    def test_dics_2(self):
        tmp = Trigger('a','b')
        tmp.setWT('ale',2)
        self.assertEqual(tmp.getWT('ale'),2.0)

    def test_dics_3(self):
        tmp = Trigger('a','b')
        tmp.setWT('ale',2.3)
        self.assertEqual(tmp.getWT('ale'),2.3)

    def test_to_string(self):
        excepted = "1980-01-06 00:00:09.200000 UTC (GPS Time: 0.2) SNR:21.0 SNRMax:0.0 Freq:0.0 MAXFreq:0.0 Duration:0.0 Wave:- NCoef:512"
        tmp = Trigger('a','b',0.2, 21)
        tmp.uuid= '4e2ecc50-91ba-4ef9-8ab9-45a852f8e3c1'
        self.assertEqual(excepted, str(tmp))

    def test_equal(self):
        a = Trigger('a','b')
        b = copy.deepcopy(a)
        self.assertEqual(a,b)
