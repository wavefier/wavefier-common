from unittest import TestCase

from wavefier_common.kafka.avro.Message import Message
from wavefier_common.Trigger import Trigger
from wavefier_common.param.WitheningParam import WitheningParam


class TestMessage(TestCase):

    def test_creation_ko1(self):
        try:
            message = Message()
            self.fail()
        except Exception as ex:
            self.assertTrue(True)

    def test_creation_ko2(self):
        try:
            message = Message('a')
            self.fail()
        except Exception as ex:
            self.assertTrue(True)

    def test_creation_ko3(self):
        try:
            message = Message('a', 'b')
            self.fail()
        except Exception as ex:
            self.assertTrue(True)

    def test_creation_ok(self):
        try:
            message = Message('topic1', Trigger('a', 'b'))
            self.assertTrue(True)
        except Exception as ex:
            self.fail(ex)

    def test_creation_ok1(self):
        try:
            message = Message('topic1', Trigger('a', 'b'),
                              force_object_schema='dummy')
            self.assertEqual(message.get_value_schema(), 'dummy')
        except Exception as ex:
            self.fail(ex)

    def test_creation_ko4(self):
        try:
            message = Message('topic1', object=Trigger('a', 'b'), key='a')
            self.fail()
        except Exception as ex:
            self.assertTrue(True)

    def test_creation_ok2(self):
        try:
            message = Message('topic1', object=Trigger(
                'a', 'b'), key=Trigger('a', 'b'))
            self.assertTrue(True)
        except Exception as ex:
            self.fail()

    def test_creation_ok3(self):
        try:
            message = Message('topic1', object=Trigger(
                'a', 'b'), key=Trigger('a', 'b'), force_key_schema='dummy')
            self.assertEqual(message.get_key_schema(), 'dummy')
        except Exception as ex:
            self.fail()

    def test_creation_ok4(self):
        try:
            message = Message('topic1', object=Trigger('a', 'b'))
            self.assertEqual(message.get_value_schema(),
                             Message.load_avro_schema(Trigger('a', 'b')))
        except Exception as ex:
            self.fail()

    def test_get_key(self):
        t0 = 'topic'
        s1 = Trigger('a', 'b')
        s2 = "Test1"
        t1 = Trigger('a', 'b')
        t2 = "Test2"
        msg = Message(t0, t1, t2, s1, s2)
        self.assertEqual(msg.get_key(), s1)

    def test_get_key_schema(self):
        t0 = 'topic'
        t1 = Trigger('a', 'b')
        t2 = "Test1"
        s1 = Trigger('a', 'b')
        s2 = "Test2"
        msg = Message(t0, t1, t2, s1, s2)
        self.assertEqual(msg.get_key_schema(), s2)

    def test_get_value(self):
        t0 = 'topic'
        t1 = Trigger('a', 'b')
        t2 = "Test1"
        s1 = Trigger('a', 'b')
        s2 = "Test2"
        msg = Message(t0, t1, t2, s1, s2)
        self.assertEqual(msg.get_value(), t1)

    def test_get_value_schema(self):
        t0 = 'topic'
        t1 = Trigger('a', 'b')
        t2 = "Test1"
        s1 = Trigger('a', 'b')
        s2 = "Test2"
        msg = Message(t0, t1, t2, s1, s2)
        self.assertEqual(msg.get_value_schema(), t2)

    def test_get_topic(self):
        t0 = 'topic'
        t1 = Trigger('a', 'b')
        t2 = "Test1"
        s1 = Trigger('a', 'b')
        s2 = "Test2"
        msg = Message(t0, t1, t2, s1, s2)
        self.assertEqual(msg.get_topic(), t0)

    def test_serialize_object(self):

        class Test1(object):
            def __init__(self):
                self.a = 0
                self.b = 'dummy'

        iT = Test1()
        dic = Message.dic_from_object(iT)

        self.assertEqual(dic['a'], iT.a)
        self.assertEqual(dic['b'], iT.b)

    def test_serialize_object2(self):

        class Test1(object):
            def __init__(self):
                self.a = 0
                self.b = 'dummy'

            def set_pippo(self, pippo):
                self.c = pippo

        iT = Test1()
        iT.set_pippo('pluto')

        dic = Message.dic_from_object(iT)

        self.assertEqual(dic['a'], iT.a)
        self.assertEqual(dic['b'], iT.b)
        self.assertEqual(dic['c'], iT.c)

    def test_deserialize_object(self):

        class Test1(object):
            def __init__(self):
                self.a = 0
                self.b = 'dummy'

            def set_pippo(self, pippo):
                self.c = pippo

            def make_from_dic(self, dictionary):
                object = Test1()
                for key in dictionary:
                    setattr(object, key, dictionary[key])
                return object


        iT = Test1()
        iT.set_pippo('pluto')

        dic = Message.dic_from_object(iT)

        self.assertEqual(dic['a'], iT.a)
        self.assertEqual(dic['b'], iT.b)
        self.assertEqual(dic['c'], iT.c)

        iMessage = Message("t1", Test1(),"-")
        iT2 = iMessage.object_from_dic(dic)

        self.assertEqual(iT2.a, iT2.a)
        self.assertEqual(iT2.b, iT2.b)
        self.assertEqual(iT2.c, iT2.c)

    def test_serialize_WTH(self):

        iT = WitheningParam(2)
        dic = Message.dic_from_object(iT)

        self.assertEqual(dic['AR'], iT.AR)
        self.assertEqual(dic['uuid'], iT.uuid)

    def test_get_serialized_key(self):

        class Test1(object):

            def __init__(self):
                self.a = 0
                self.b = 'dummy'
                self.c = {}
                self.d = None

            def set_pippo(self, ctl):
                self.ctl = ctl

            def get_pippo(self):
                return self.ctl

            def make_from_dic(self, dic):
                pass

        iT = Test1()
        iT.set_pippo('pluto')

        message = Message('topic1', object=iT, force_object_schema='-')

        dic = message.get_value(Message.FIELD_FORMAT.DICT)

        dic2 = message.get_key(Message.FIELD_FORMAT.DICT)

        self.assertEqual(dic2, None)
        self.assertEqual(dic['a'], iT.a)
        self.assertEqual(dic['b'], iT.b)
        self.assertEqual(dic['c'], iT.c)
        self.assertEqual(dic['d'], iT.d)
        self.assertEqual(dic['ctl'], iT.get_pippo())

    def test_Equals(self):
        class Test1(object):

            def __init__(self):
                self.a = 0
                self.b = 'dummy'
                self.c = {}
                self.d = None

            def set_pippo(self, ctl):
                self.ctl = ctl

            def get_pippo(self):
                return self.ctl

            def make_from_dic(self, dic):
                pass

        iT = Test1()

        message = Message('topic1', object=iT, force_object_schema='-')
        message2 = Message('topic1', object=iT, force_object_schema='-')

        self.assertEqual(message, message2)
