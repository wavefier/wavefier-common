from unittest import TestCase
import tests
from uuid import uuid4


from wavefier_common.Trigger import Trigger

from confluent_kafka import avro
from wavefier_common.util.Path import Path

from wavefier_common.kafka.avro.Message import Message
from wavefier_common.param.WitheningParam import WitheningParam


from confluent_kafka.avro import AvroProducer

b="""
    {
        "namespace": "confluent.io.examples.serialization.avro",
        "name": "User",
        "type": "record",
        "fields": [
            {"name": "name", "type": "string"},
            {"name": "favorite_number", "type": "int"},
            {"name": "favorite_color", "type": "string"}
        ]
    }
"""
a="""
{
        "namespace": "trigger.common.wavefier.param",
        "type": "record",
        "name": "WitheningParam",
        "fields": [
                {
                        "name": "uuid",
                        "type": "string"
                },
                {
                        "name": "AR",
                        "type": "int",
                        "default": 3000
                }
        ]
}
"""

record_schema = avro.loads(b)


class User(object):
    """
        User stores the deserialized user Avro record.
    """

    # Use __slots__ to explicitly declare all data members.
    __slots__ = ["name", "favorite_number", "favorite_color", "id"]

    def __init__(self, name=None, favorite_number=None, favorite_color=None):
        self.name = name
        self.favorite_number = favorite_number
        self.favorite_color = favorite_color
        # Unique id used to track produce request success/failures.
        # Do *not* include in the serialized object.
        self.id = uuid4()

    def to_dict(self):
        """
            The Avro Python library does not support code generation.
            For this reason we must provide a dict representation of our class for serialization.
        """
        return {
            "name": self.name,
            "favorite_number": self.favorite_number,
            "favorite_color": self.favorite_color
        }


def on_delivery(err, msg, obj):
    """
        Handle delivery reports served from producer.poll.
        This callback takes an extra argument, obj.
        This allows the original contents to be included for debugging purposes.
    """
    if err is not None:
        print('Message delivery failed with error {}'.format(err))
    else:
        print('Message successfully produced to {} [{}] at offset {}'.format( msg.topic(), msg.partition(), msg.offset()))


class KafkaAvroProducerTest(TestCase):

    def test_vanilla_kafka1(self):
        """
            Produce User records
        """
        # handle common configs
        conf = {'bootstrap.servers': tests.kafka_broker,
                'schema.registry.url': tests.kafka_schema_registry}

        topic = "topic_for_Vanilla_test"

        producer = AvroProducer(conf, default_value_schema=record_schema)

        # Instantiate new User, populate fields, produce record, execute callbacks.
        record = User()
        try:
            record.name = "UserName"
            record.favorite_number = 3
            record.favorite_color = "red"

            # The message passed to the delivery callback will already be serialized.
            # To aid in debugging we provide the original object to the delivery callback.
            producer.produce(topic=topic, value=record.to_dict(),
                             callback=lambda err, msg, obj=record: on_delivery(err, msg, obj))
            # Serve on_delivery callbacks from previous asynchronous produce()
            producer.poll(0)

        except ValueError:
            print("Invalid input, discarding record...")

        print("\nFlushing records...")
        producer.flush()

    def _test_Chekc_avro_schema_vanilla_kafka_with_message(self):
        """
            Produce User records
        """
        # handle common configs
        conf = {'bootstrap.servers': tests.kafka_broker,
                'schema.registry.url': tests.kafka_schema_registry}

        iObject = WitheningParam()

        iMessage = Message('test_topic', iObject)



        try:

            print(">>> TOPIC:      " + str(iMessage.get_topic()))
            print(">>> DICTIONARY: " + str(Message.dic_from_object(iMessage)))
            print(">>> SCHEMA A:     " + str(iMessage.get_value_schema()))


            myObject = { "uuid" : "12345" , "AR": "12" }
           # myschema = avro.loads(str(iMessage.get_value_schema()))
            myschema = avro.loads(a)


            #####################
            producer = AvroProducer(conf, default_value_schema=myschema)

            # The message passed to the delivery callback will already be serialized.
            # To aid in debugging we provide the original object to the delivery callback.
            producer.produce(topic=iMessage.get_topic(), value=myObject,
                             callback=lambda err, msg, obj=myObject: on_delivery(err, msg, obj))
            # Serve on_delivery callbacks from previous asynchronous produce()
            producer.poll(0)

        except ValueError:
            print("Invalid input, discarding record...")

        print("\nFlushing records...")
        producer.flush()
