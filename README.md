# Common Wavefier Library
This library has the intent to contain all the common part used in the wavefier project.

## Usefull commands


### Create Source dist build
To create a source distribution, you run: `python setup.py sdist`. It create a tar gz on a directory dist with a source distribution

### Create Binary dist build
To create a binary distribution called a wheel, you run: `python setup.py bdist`

Other opions can be used:
* `python setup.py bdist --formats=rpm` rpm Format
* `python setup.py bdist --formats=wininst` window installer 

## Development
Setup the environment:
* In the directory `tools/kafka` run the command './run-dev-env.sh'
* Develop as you know
If you whanto to develop via docker: create the docker immage for the develop with the command  ` docker build -t wth-env .` Run the develop bash with `docker run -it -v $(pwd):/app wth-env /bin/bash`



### Run test in docker
Setup the environment with the follow steps:
```
#> cd tools/kafka && docker-compose up -d && cd ../..
#> docker run --network=kafka_kafka_network_test --link=kafka_test:kafka_test --link=kafka-schema-registry_test:kafka-schema-registry_test $CONTAINER_TEST_IMAGE python setup.py nosetests --with-coverage --cover-erase --cover-html --cover-html-dir=docs/_build/coverage
#> cd tools/kafka && docker-compose down && cd ../..

```

### Run test outside docker    
Setup the environment:
* In the directory `tools/kafka` run the command `./run-dev-env.sh`
* To run the test just run `python setup.py test` on home directory 

### Run Documentation
To create documentation use the `make clean && make docs`. On the directory docs you can see the generated documentation
